const express = require("express");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false}));

app.get("/", (req, resp) => {
    resp.status(200).send("Hello Node API World");
    resp.end();
});

app.get("/say-hi", (req, resp) =>{
    const name = req.query.name;
    resp.status(200).send(`Hello ${name}, Welcome to Node API Development`).end();
})

app.post("/add-person", (req, resp)=>{

    const newPerson = {
        ...req.body,
        address: {
            street: "123 Street",
            city: "Cool City"
        }
    }
    return resp.json(newPerson);
    
});

app.listen(3000, ()=>{
    console.log("Node server started on port 3000...");
});
