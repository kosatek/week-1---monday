// IIFE
(function (win) {

    // Game Object
    const Game = function () {
        this.gameover = false;
        this.winner = null;
        this.didPlayerWin = function() {
            return this.winner === 'player';
        }
    };

    // Enemy object
    const enemy = {
        health: 15,
        attack: function () {
            player.health -= 2;
        }
    }

    // Player object
    const player = {
        name: '',
        health: 20,
        takeWeapon: function (weapon) {
            this.weapon = weapon;
        },
        attack: function () {
            if (game.gameover === false) {
                let damage = 0;

                switch (this.weapon) {
                    case 'sword':
                        damage = 2;
                        break;
                    case 'pistol':
                        damage = 3;
                        break;
                    case 'knife':
                        damage = 1;
                        break;
                    case 'fists':
                        damage = 0;
                        break;
                }
                enemy.health -= damage;
                enemy.health = enemy.health < 0 ? 0 : enemy.health;
                let result = `${this.name} did ${damage} damage! The enemy has ${enemy.health} health left!`
                if (damage === 0) { // No effect! 
                    enemy.attack();
                    result = `${this.name} tried to attack the enemy with his bare hands It had no effect! The enemy did a counter attack! You have ${this.health} left!`;
                }
                if (enemy.health === 0) {
                    game.gameover = true;
                    game.winner = 'player';
                } else if (this.health === 0) {
                    game.gameover = true;
                    result = 'You have lost! The enemy overtakes the ship and steals all your gold!'
                    game.winner = 'enemy';
                }
                return result;
            }
        }
    }

    Game.prototype.enemy = enemy;
    Game.prototype.player = player;
    win.game = new Game();

})(window);

function onNameChange(){
    game.player.name = document.getElementById("name").value;

    const p = document.getElementById("new-name");

    p.innerText = ("Your player name will be " + game.player.name);
}

function acceptName(){

    document.getElementById("section-player-name").style.display = "none";
    document.getElementById("player-info").innerText = game.player.name;
    document.getElementById("intro").style.display = "none";
    document.getElementById("game").style.display = "block";
}

function takeWep(){
    game.player.takeWeapon(document.getElementById("weapon").value);
    document.getElementById("player-info").innerText = (game.player.name + " with a " + game.player.weapon);
}

function attack(){
    document.getElementById("game-info").innerText = game.player.attack();
    if(game.winner === "player"){
        alert("You won! And you have defended the ship! Good work!!");
        document.getElementById("game-info").innerText = "You won! Good work!";
    }
    else if(game.winner === "enemy"){
        alert("You lost... :(");
        document.getElementById("game-info").innerText = "You lost... Better luck next time";
    }
}