const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use((req, resp, next)=>{
    let authenticated = true;
    
    if(authenticated){
        next();
    }
    else{
        resp.json({
            error: "Not authenticated"
        });
    }
})

const kitchenRoutes = require("./routes/kitchen");

app.use("/kitchen", kitchenRoutes);

app.listen(port, ()=>{
    console.log(`Server running on port ${port}...`);  
});