const express = require("express");
const router = express.Router();

const kitchenAppliance = require("../models/kitchenAppliance");

router.get("/", (req, resp)=>{
    const apiResp = kitchenAppliance.getAppliances();
    resp.json(apiResp).end();
})

router.post("/add", (req, resp)=>{
    resp.send("Add a kitchen appliance").end();
})

router.put("/update", (req, resp)=>{
    resp.send("Updating a kitchen appliance").end();
})

router.delete("/delete", (req, resp)=>{
    resp.send("Deleting a kitchen appliance").end();
})

module.exports = router;