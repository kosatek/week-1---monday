const ApiResponse = require("./ApiResponse");

class KitchenAppliance {

    getAppliances(){
        const apiResp = new ApiResponse();
        apiResp.setData(["Kettle", "Fridge", "Bean Grinder", "Air Fryer"]);
        return apiResp;
    }

    addAppliance(){

    }

    updateAppliance(){

    }

    deleteAppliance(){

    }
}

module.exports = new KitchenAppliance;