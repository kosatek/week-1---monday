Kitchen API Planning
====================

* Use Express
* Use Express Router
* Use a custom response object
* Implement Middleware - Use next()
* Make use of ES6 Classes
* Follow the module design - Using exports

# The Kitchen API

## Appliance Endpoints

* [GET] /api/v1/kitchen/appliances
* [POST] /api/v1/kitchen/appliances/add
* [PUT] /api/v1/kitchen/appliances/update
* [DELETE] /api/v1/kitchen/appliances/delete