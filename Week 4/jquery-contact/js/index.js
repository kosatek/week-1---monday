let $resultName = null;
let $resultEmail = null;
let $resultReason = null;
let $resultComments = null;

$(document).ready(function(){

    //jQuery is ready to use
    $resultName = $("#result-name");
    $resultEmail = $("#result-email");
    $resultReason = $("#result-reason");
    $resultComments = $("#result-comments");


    $("#name").keyup(function(e){
        console.log(e.target.value);
        $resultName.text(e.target.value);
    })

    $("#email").keyup(function(e){
        console.log(e.target.value);
        $resultEmail.text(e.target.value);
    })

    $("#reason").change(function(e){
        console.log(e.target.value);
        $resultReason.text(e.target.value);
    })

    $("#comments").keyup(function(e){
        console.log(e.target.value);
        $resultComments.text(e.target.value);
    })

    $("#btn-submit").click(function(e){
        e.preventDefault();
    })
})