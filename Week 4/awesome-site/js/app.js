const elOrderList = document.getElementById("order-list");
const elMenuItems = document.getElementById("menu");
const order = {
    items: []
};

function onMenuChanged(){
    console.log("Menu has changed");
}

function addToOrder(){
    //Get the value from the select
    const menuItem = elMenuItems.value;

    //Check that the user selects somthing
    if(menuItem == -1){
        alert("Please select something from the menu");
    }
    else{
        //Add the item to our order
        order.items.push(menuItem);
        updateOrder();
    }
}

function clearOrder(){
    order.items = [];
    updateOrder();
}

function updateOrder(){

    elOrderList.innerHTML = "";

    if(order.items.length === 0){
        elOrderList.innerHTML = "<li>404 Food not found!</li>";
        return;
    }

    order.items.forEach(item => {
        const elItem = document.createElement("li");
        elItem.innerText = item;
        elOrderList.appendChild(elItem);
    });
}