function getId(){
    let url = window.location.search;

    let id = url.split('=');

    return id[1];
}

async function getComments(){
    const elComments = document.getElementById("comments");

    try{
        const response = await Poster.getComments();

        const filteredComments = response.filter(resp => resp.postId == getId());

        filteredComments.forEach(comment => {
            elComments.appendChild(Poster.createCommentTemplate(comment));
        });
    }
    catch(e){
        console.error(e);
    }
}

document.addEventListener("DOMContentLoaded", () => {
    getComments();
})