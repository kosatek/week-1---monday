module.exports = async function (context, req) {
    
    const playerHand = req.query.hand;
    const hands = ['Rock', 'Paper', 'Scissors'];

    if (playerHand) {
        //1. Generate a CPU hand
        //2. Compare hands
        //3. Respond correctly

        const cpuHand = hands[Math.floor(Math.random() * 3)];
        
        let result = 'Welcome to Rock, Paper, Scissors';

        if(playerHand === cpuHand){
            result = `It's a draw! You both played ${playerHand}`;
        }
        else if(cpuHand === hands[0]){
            if(playerHand === hands[2]){
                result = `It's a loss... You played ${playerHand}, whilst the CPU played ${cpuHand}`;
            }
            else if(playerHand === hands[1]){
                result = `It's a win! You played ${playerHand}, whilst the CPU played ${cpuHand}`;
            }
        }
        else if(cpuHand === hands[1]){
            if(playerHand === hands[0]){
                result = `It's a loss... You played ${playerHand}, whilst the CPU played ${cpuHand}`;
            }
            else if(playerHand === hands[2]){
                result = `It's a win! You played ${playerHand}, whilst the CPU played ${cpuHand}`;
            }
        }
        else if(cpuHand === hands[2]){
            if(playerHand === hands[1]){
                result = `It's a loss... You played ${playerHand}, whilst the CPU played ${cpuHand}`;
            }
            else if(playerHand === hands[0]){
                result = `It's a win! You played ${playerHand}, whilst the CPU played ${cpuHand}`;
            }
        }

        context.res = {
            // status: 200, /* Defaults to 200 */
            body: result
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Please pass a Rock-Paper-Scissors hand on the query string or in the request body"
        };
    }
};