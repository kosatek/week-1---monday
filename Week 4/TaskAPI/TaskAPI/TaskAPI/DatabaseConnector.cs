using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;
using TaskAPI.Models;

namespace TaskAPI
{
    public class DatabaseConnector
    {
        private DocumentClient client;

        private async Task InitDB()
        {
            this.client = new DocumentClient(new Uri(ConfigurationManager.AppSettings["accountEndpoint"]), ConfigurationManager.AppSettings["accountKey"]);
            await this.client.CreateDatabaseIfNotExistsAsync(new Database { Id = "TodoAPI" });
            await this.client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri("TodoAPI"), new DocumentCollection { Id = "Todo" });
        }

        public async Task<List<TodoItem>> GetAllTodos(){
            await InitDB();
            IQueryable<TodoItem> todos = this.client.CreateDocumentQuery<TodoItem>(UriFactory.CreateDocumentCollectionUri("TodoAPI", "Todo"));
            return todos.ToList();
        }

        public async Task<TodoItem> GetTodoById(string id){
            await InitDB();
            var response = await client.ReadDocumentAsync(UriFactory.CreateDocumentUri("TodoAPI", "Todo", id), new RequestOptions{PartitionKey = new PartitionKey(id)});
            var todo = JsonConvert.DeserializeObject<TodoItem>(response.Resource.ToString());
            return todo;
        }

        public async Task PostTodo(TodoItem todo){
            await InitDB();

            await client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri("TodoAPI", "Todo"), todo);
        }

        public async Task PutTodo(TodoItem todo){
            await InitDB();

            await client.UpsertDocumentAsync(UriFactory.CreateDocumentCollectionUri("TodoAPI", "Todo"), todo);
        }
    }
}