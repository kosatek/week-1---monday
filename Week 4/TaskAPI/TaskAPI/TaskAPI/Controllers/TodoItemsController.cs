﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaskAPI.Models;

namespace TaskAPI.Controllers
{
    [Route("api/")]
    [ApiController]
    public class TodoItemsController : ControllerBase
    {
        private DatabaseConnector _connector;

        public TodoItemsController()
        {
            _connector = new DatabaseConnector();
        }

        // GET: api/
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoItem>>> GetTodoItems()
        {
            return await _connector.GetAllTodos();
        }

        // GET: api/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TodoItem>> GetTodoItem(string id)
        {
            var todoItem = await _connector.GetTodoById(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }

        // PUT: api/5
        [HttpPut("{id}")]
        public async Task<string> PutTodoItem(TodoItem todoItem)
        {
            await _connector.PutTodo(todoItem);
            return "Success";
        }

        // POST: api/TodoItems
        [HttpPost]
        public async Task<ActionResult<TodoItem>> PostTodoItem(TodoItem todoItem)
        {
            await   _connector.PostTodo(todoItem);

            return CreatedAtAction("GetTodoItem", new { id = todoItem.Id }, todoItem);
        }
    }
}
