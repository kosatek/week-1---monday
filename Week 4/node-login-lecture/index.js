const express = require("express");
const path = require("path");
const port = process.env.PORT || 5000;
const session = require("express-session");
const app = express();


app.use(session({
    secret: "asdfasdf",
    resave: true,
    saveUninitialized: false,
    cookie: {
        maxAge: 8 * 60 * 60 * 1000
    }
}))
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/public", express.static( path.join( __dirname, "app", "static" ) ));

/**--------------------------------------------------------
 * VIEW ROUTES
 --------------------------------------------------------*/

app.get("/", (req, resp)=>{
    resp.sendfile( path.join( __dirname, "app", "views", "login.html" ) );
})

app.get("/home", (req, resp)=>{
    if(req.session.loggedin){
        resp.sendfile( path.join( __dirname, "app", "views", "index.html" ) );
    }
    else{
        resp.redirect("/");
    }
})

app.get("/about", (req, resp)=>{
    resp.sendfile( path.join( __dirname, "app", "views", "about.html" ) );
})

app.get("/contact", (req, resp)=>{
    resp.sendfile( path.join( __dirname, "app", "views", "contact.html" ) );
})

/**--------------------------------------------------------
 * REQUEST
 --------------------------------------------------------*/

app.post("/auth", (req, resp)=>{
    const {username, password} = req.body;
    //SELECT username, password and check it!
    if(username === "eddie" && password === "10111"){
        
        req.session.loggedin = true;
        req.session.username = username;

        resp.json({
            loggedin: true
        }).end()
    }
    else{
        resp.json({
            loggedin: false,
            message: "Invalid username or password"
        }).end(); 
    }
})

app.listen(port, ()=>{
    console.log(`Server running on port ${port}...`);
    
})