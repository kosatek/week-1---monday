NODE + Express Login
====================

# Views (html files)
* login.html - Login form
* index.html - Home Page - Random text and a heading
* about.html - Photo, some random text
* contact.html - Contact form (not really)

# Static (css, js, images)
* css/styles.css - Some basic styling for our awesome site
* js/login.js - Handle the login form
* img/about.jpg - Image of your self

# Node + Express

## View Routes
* '/' - login.html
* '/home' - index.html
* '/about' - about.html
* '/contact' - contact.html

## Other (Misc.)
* '/auth' - [ POST ] - Validate the login credentials ( This will return JSON )