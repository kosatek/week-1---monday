var ref = document.getElementById("Unsorted");

function funk(n){
    for(var i = 1; i < n; i++){
        var newElem = document.createElement("li");
        if((i % 15) === 0){
            newElem.innerText = "Fizz Buzz";
        }
        else if((i % 5) === 0){
            newElem.innerText = "Buzz";
        }
        else if((i % 3) === 0){
            newElem.innerText = "Fizz";
        }
        else{
            newElem.innerText = i;
        }
        ref.appendChild(newElem);
    }
}

funk(20);