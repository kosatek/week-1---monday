﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Task_21_Part_2.Models;

namespace Task_21_Part_2.Controllers
{
    public class HomeController : Controller
    {
        static AnimalHub ah = new AnimalHub();
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Game()
        {
            AnimalHub ah = StartGame();
            return View(ah);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Incorrect()
        {
            ah.pts = 0;
            return View();
        }

        public IActionResult Correct()
        {
            ah.pts++;
            return View(ah);
        }

        public IActionResult Victory()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public AnimalHub StartGame()
        {
            WebClient client = new WebClient();
            Random r = new Random();
            int ranNum = r.Next(2);

            if (ranNum == 0)
            {
                var json = client.DownloadString("https://random.dog/woof.json");
                Dog dog = JsonConvert.DeserializeObject<Dog>(json);

                while (true)
                {
                    if (dog.Url.Contains(".webm") || dog.Url.Contains(".mp4"))
                    {
                        json = client.DownloadString("https://random.dog/woof.json");
                        dog = JsonConvert.DeserializeObject<Dog>(json);
                    }
                    else
                    {
                        break;
                    }
                }

                ah.imgUrl = dog.Url;
                ah.anim = 1;
            }
            else
            {
                var json = client.DownloadString("https://aws.random.cat/meow");
                Cat cat = JsonConvert.DeserializeObject<Cat>(json);

                while (true)
                {
                    if (cat.File.Contains(".webm") || cat.File.Contains(".mp4"))
                    {
                        json = client.DownloadString("https://random.dog/woof.json");
                        cat = JsonConvert.DeserializeObject<Cat>(json);
                    }
                    else
                    {
                        break;
                    }
                }

                ah.imgUrl = cat.File;
                ah.anim = 2;
            }
            return ah;
        }
    }
}
