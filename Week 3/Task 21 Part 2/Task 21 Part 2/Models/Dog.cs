﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Task_21_Part_2.Models
{
    public class Dog
    {
        string url;

        public Dog(string url)
        {
            this.Url = url;
        }

        public string Url { get => url; set => url = value; }
    }
}
