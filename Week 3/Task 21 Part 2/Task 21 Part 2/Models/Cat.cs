﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task_21_Part_2.Models
{
    public class Cat
    {
        string file;

        public Cat(string file)
        {
            this.File = file;
        }

        public string File { get => file; set => file = value; }
    }
}
