﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using YulpTask.Models;

namespace YulpTask.Controllers
{
    [Route("api/")]
    [ApiController]
    public class EstablishmentsController : ControllerBase
    {
        private readonly EstablishmentContext _context;

        public EstablishmentsController(EstablishmentContext context)
        {
            _context = context;
        }

        // GET: api/
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Establishment>>> GetEstablishments()
        {
            return await _context.Establishments.ToListAsync();
        }

        // GET: api/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Establishment>> GetEstablishment(long id)
        {
            var establishment = await _context.Establishments.FindAsync(id);

            if (establishment == null)
            {
                return NotFound();
            }

            return establishment;
        }

        //GET: api/stars/3
        [HttpGet("stars/{stars}")]
        public async Task<ActionResult<IEnumerable<Establishment>>> GetStars(int stars)
        {
            var establishment = await _context.Establishments.ToListAsync();
            List<Establishment> starEst = new List<Establishment>();

            foreach(Establishment est in establishment)
            {
                if (est.Stars >= stars)
                {
                    starEst.Add(est);
                }
            }

            if (establishment == null)
            {
                return NotFound();
            }

            return starEst;
        }

        [HttpGet("reviewer/{revName}")]
        public async Task<ActionResult<IEnumerable<Establishment>>> GetReviewer(string revName)
        {
            var establishment = await _context.Establishments.ToListAsync();
            List<Establishment> revEst = new List<Establishment>();

            foreach(Establishment est in establishment)
            {
                if(est.RevName.ToLower().Equals(revName))
                {
                    revEst.Add(est);
                }
            }
            
            if(establishment == null)
            {
                return NotFound();
            }

            return revEst;
        }

        [HttpGet("anon")]
        public async Task<ActionResult<IEnumerable<Establishment>>> GetAnon()
        {
            var establishment = await _context.Establishments.ToListAsync();
            List<Establishment> anonEst = new List<Establishment>();

            foreach(Establishment est in establishment)
            {
                if(est.RevName.Equals("") || est.RevName.Equals(null))
                {
                    est.RevName = "Anonymous";
                    anonEst.Add(est);
                }
            }

            if(establishment == null)
            {
                return NotFound();
            }

            return anonEst;
        }

        // PUT: api/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEstablishment(long id, Establishment establishment)
        {
            if (id != establishment.Id)
            {
                return BadRequest();
            }

            _context.Entry(establishment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstablishmentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/
        [HttpPost]
        public async Task<ActionResult<Establishment>> PostEstablishment(Establishment establishment)
        {
            _context.Establishments.Add(establishment);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEstablishment", new { id = establishment.Id }, establishment);
        }

        // DELETE: api/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Establishment>> DeleteEstablishment(long id)
        {
            var establishment = await _context.Establishments.FindAsync(id);
            if (establishment == null)
            {
                return NotFound();
            }

            _context.Establishments.Remove(establishment);
            await _context.SaveChangesAsync();

            return establishment;
        }

        private bool EstablishmentExists(long id)
        {
            return _context.Establishments.Any(e => e.Id == id);
        }
    }
}
