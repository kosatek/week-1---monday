﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace YulpTask.Models
{
    public class EstablishmentContext: DbContext
    {
        public EstablishmentContext(DbContextOptions<EstablishmentContext> options): base(options)
        {
        }

        public DbSet<Establishment> Establishments { get; set; }
    }
}
