﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;


namespace Task_21_Part_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            pictureBox.Load("https://i.imgur.com/4g5qEzw.jpg");
        }

        private async void Dog_Click(object sender, EventArgs e)
        {
            string json = await getImg("https://random.dog/woof.json");
            Dog dog = JsonConvert.DeserializeObject<Dog>(json);
            while (true)
            {
                if (dog.Url.Contains(".webm") || dog.Url.Contains(".mp4"))
                {
                    json = await getImg("https://random.dog/woof.json");
                    dog = JsonConvert.DeserializeObject<Dog>(json);
                }
                else
                {
                    break;
                }
            }
            pictureBox.Load(dog.Url); 
        }
        private async void Cat_Click(object sender, EventArgs e)
        {
            string json = await getImg("https://aws.random.cat/meow");
            Cat cat = JsonConvert.DeserializeObject<Cat>(json);
            while (true)
            {
                if (cat.File.Contains(".webm") || cat.File.Contains(".mp4"))
                {
                    json = await getImg("https://aws.random.cat/meow");
                    cat = JsonConvert.DeserializeObject<Cat>(json);
                }
                else
                {
                    break;
                }
            }
            pictureBox.Load(cat.File);
        }
        private async Task<string> getImg(string path)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(path);
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}
