﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21_Part_1
{
    class Dog
    {
        string url;

        public Dog(string url)
        {
            this.Url = url;
        }

        public string Url { get => url; set => url = value; }
    }
}
