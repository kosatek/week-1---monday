﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21_Part_1
{
    class Cat
    {
        string file;

        public Cat(string file)
        {
            this.File = file;
        }

        public string File { get => file; set => file = value; }
    }
}
