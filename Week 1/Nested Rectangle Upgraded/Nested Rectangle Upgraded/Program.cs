﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nested_Rectangle_Upgraded
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the dimensions you want the square to be");
            int col = SetCol();
            int row = SetRow();

            string[,] rectangle = MakeRectangle(col, row);

            PrintRectangle(rectangle);
        }

        static int SetCol()
        {
            Console.Write("Column length: ");
            Int32.TryParse(Console.ReadLine(), out int col);

            return col;
        }

        static int SetRow()
        {
            Console.Write("Row length: ");
            Int32.TryParse(Console.ReadLine(), out int row);

            return row;
        }

        static string[,] MakeRectangle(int col, int row)
        {
            string[,] rectangle = new string[col, row];

            for (int i = 0; i < col; i++)
            {
                for (int j = 0; j < row; j++)
                {
                    if (i == 0 || i == col - 1)
                    {
                        rectangle[i, j] = "#";
                    }
                    else if (i == 2 || i == col - 3)
                    {
                        rectangle[i, j] = "#";
                        if (j == 1 || j == row - 2)
                        {
                            rectangle[i, j] = " ";
                        }

                    }
                    else if (j == 2 || j == row - 3)
                    {
                        rectangle[i, j] = "#";
                        if (i == 1 || i == col - 2)
                        {
                            rectangle[i, j] = " ";
                        }
                    }
                    else
                    {
                        rectangle[i, 0] = "#";

                        if (j != 0 && j != row - 1)
                        {
                            rectangle[i, j] = " ";
                        }
                        rectangle[i, row - 1] = "#";
                    }
                }
            }
            return rectangle;
        }

        static void PrintRectangle(string[,] rectangle)
        {
            for (int i = 0; i < rectangle.GetLength(0); i++)
            {
                for (int k = 0; k < rectangle.GetLength(1); k++)
                {
                    Console.Write(rectangle[i, k]);
                }
                Console.WriteLine();
            }
        }
    }
}
