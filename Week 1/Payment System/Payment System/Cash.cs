﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_System
{
    class Cash
    {
        private int amount;
        public Cash(int amount)
        {
            this.amount = amount;
        }

        public int Amount { get => amount; set => amount = value; }
    }
}
