﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_System
{
    class CreditCard : Card
    {
        private int balance;
        public CreditCard(int cardNum, string bank, int balance) : base(cardNum, bank)
        {
            CardNum = cardNum;
            Bank = bank;
            Balance = balance;
        }

        public int CardNum { get; }
        public int Balance { get => balance; set => balance = (value+25); } //Added so that credit cards pay a little fee for usage
        //And thats the unique attribute that credit cards have over normal cards and debit

    }
}
