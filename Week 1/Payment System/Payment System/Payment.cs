﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_System
{
    class Payment
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to PayEd!");

            DebitCard dc = new DebitCard(123, "DNB", 1000);
            CreditCard cc = new CreditCard(321, "Sparebank 1", 5000);
            Cash ch = new Cash(12000);

            while (true)
            {
                Console.WriteLine("-----------------------------------------");

                Console.WriteLine("Choose an item to purchase:");
                Console.WriteLine("1 - Fanta Shokata (30kr)");
                Console.WriteLine("2 - Mightymug (180kr)");
                Console.WriteLine("3 - Samsung Galaxy Note 9 (10000kr)");
                Console.WriteLine("Hit anything else to exit the program");

                Int32.TryParse(Console.ReadLine(), out int productChoise);

                Console.WriteLine("How would you like to pay?");
                Console.WriteLine("\n1 - Card");
                Console.WriteLine("2 - Cash");
                Console.WriteLine("Anything else - Exit/Cancel order");
                Console.Write("\nChoose a method of payment: ");
                Int32.TryParse(Console.ReadLine(), out int choise);

                if(choise == 1)
                {
                    Console.WriteLine("\n1 - Debit Card");
                    Console.WriteLine("\n2 - Credit Card");
                    Int32.TryParse(Console.ReadLine(), out int cardChoise);

                    if(cardChoise == 1)
                    {
                        Console.WriteLine("\nDebit Card: " + dc.CardNum);
                        Console.WriteLine("-----------------------------------------");
                        Console.WriteLine("Current balance: " + dc.Balance);
                        Console.WriteLine("-----------------------------------------");
                        dc.Balance = Purchase(productChoise, dc.Balance);
                        Console.WriteLine("New balance: " + dc.Balance);
                    }
                    else if(cardChoise == 2)
                    {
                        Console.WriteLine("\nCredit Card: " + cc.CardNum);
                        Console.WriteLine("-----------------------------------------");
                        Console.WriteLine("Current balance: " + cc.Balance);
                        Console.WriteLine("-----------------------------------------");
                        cc.Balance = Purchase(productChoise, cc.Balance);
                        Console.WriteLine("New balance: " + cc.Balance);
                    }
                }
                else if(choise == 2)
                {
                    Console.WriteLine("Cash: " + ch.Amount);
                    Console.WriteLine("-----------------------------------------");
                    Console.WriteLine("Current balance: " + ch.Amount);
                    Console.WriteLine("-----------------------------------------");
                    ch.Amount = Purchase(productChoise, ch.Amount);
                    Console.WriteLine("New balance: " + ch.Amount);
                }
                else
                {
                    break;
                }
            }
        }
        static int Purchase(int prod, int balance)
        {
            if(prod == 1)
            {
                if((balance - 30) >= 0)
                {
                    Console.WriteLine("Purchase successful!");
                    return balance -= 30;
                }
                Console.WriteLine("Not enough money!");
                return balance;
            }
            else if(prod == 2)
            {
                if(balance - 180 >= 0)
                {
                    Console.WriteLine("Purchase successful!");
                    return balance -= 180;

                }
                Console.WriteLine("Not enough money!");
                return balance;
            }
            else if(prod == 3)
            {
                if ((balance - 10000) >= 0)
                {
                    Console.WriteLine("Purchase successful!");
                    return balance -= 10000;
                }
                Console.WriteLine("No soup for you!"); //Get it?
                return balance;
            }
            else
            {
                Console.WriteLine("Unavailable product, please try again!");
                return balance;
            }
        }
    }
}
