﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_System
{
    class DebitCard : Card
    {
        private int balance;
        public DebitCard(int cardNum, string bank, int balance) : base(cardNum, bank)
        {
            CardNum = cardNum;
            Bank = bank;
            Balance = balance;
        }

        public int CardNum { get; }

        public int Balance { get => balance; set => balance = value; }
    }
}
