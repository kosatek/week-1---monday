﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment_System
{
    class Card
    {
        private int cardNum;
        private string bank;

        protected int CardNum { get => cardNum; set => cardNum = value; }
        protected string Bank { get => bank; set => bank = value; }

        public Card(int cardNum, string bank)
        {
            this.cardNum = cardNum;
            this.bank = bank;
        }

    }
}
