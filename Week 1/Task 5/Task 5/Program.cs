﻿using System;
using System.Collections.Generic;

namespace Task_5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> names = new List<string>();

            names.Add("Eddie Murphy");
            names.Add("Tom Cruise");
            names.Add("Eddie Redmayne");
            names.Add("Terry Crews");
            names.Add("Rob Schneider");

            Console.WriteLine("\n1 - Search for a name\n2 - Display all names");
            Int32.TryParse(Console.ReadLine(), out int choise);

            if (choise == 1)
            {
                Console.Write("Search: ");
                string name = Console.ReadLine();
                bool found = false;

                foreach (string txt in names)
                {
                    if (txt.Contains(name))
                    {
                        Console.WriteLine(txt);
                        found = true;
                    }
                }
                if (!found)
                {
                    Console.WriteLine("There are no names in the list that matches");
                }
            }
            else
            {
                foreach(string txt in names)
                {
                    Console.WriteLine(txt);
                }
            }
        }
    }
}
