﻿using System;
using System.Collections;

namespace Task_3___Noroff
{
    class Program
    {
        static void Main(string[] args)
        {
            bool check = true;

            //Skipping the initial 0 and 1 because it looks better, imo
            int s = 1;
            int j = 2;
            Console.WriteLine(s);
            Console.WriteLine(j);

            ArrayList tallRekke = new ArrayList();
            tallRekke.Add(s);
            tallRekke.Add(j);

            while (check)
            {
                Console.WriteLine(s + j);

                s = s + j;
                tallRekke.Add(s);

                j = s + j;
                tallRekke.Add(j);
                Console.WriteLine(j);

                if(j > 4000000 || s > 4000000)
                {
                    check = false;
                }
            }

            int sumEven = 0;

            foreach(int num in tallRekke)
            {
                if(num % 2 == 0)
                {
                    //Test to see if it actually looks at even numbers only
                    //Console.WriteLine("Even number: " + num);
                    sumEven += num;
                }
            }

            Console.WriteLine("The sum of all even numbers in the Fibonacci sequence is: " + sumEven);
        }
    }
}
