﻿using System;
using System.Collections.Generic;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter the dimensions you want the square to be: ");
            Int32.TryParse(Console.ReadLine(), out int squareDim);
            string[,] square = new string[squareDim, squareDim];

            square = GenerateSquare(square, squareDim);

            PrintSquare(square);
        }

        static void PrintSquare(string[,] square)
        {
            for (int i = 0; i < square.GetLength(0); i++)
            {
                for (int k = 0; k < square.GetLength(1); k++)
                {
                    Console.Write(square[i, k]);
                }
                Console.WriteLine();
            }
        }

        static string[,] GenerateSquare(string[,] square, int squareDim)
        {
            for (int i = 0; i < squareDim; i++)
            {
                for (int j = 0; j < squareDim; j++)
                {
                    if (i == 0 || i == squareDim - 1)
                    {
                        square[i, j] = "#";
                    }
                    else
                    {
                        square[i, 0] = "#";

                        if (j != 0 || j != squareDim - 1)
                        {
                            square[i, j] = " ";
                        }
                        square[i, squareDim - 1] = "#";
                    }
                }
            }

            return square;
        }
    }
}
