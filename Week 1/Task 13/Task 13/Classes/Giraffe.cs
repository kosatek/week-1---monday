﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13.Classes
{
    class Giraffe : Herbivore, IWalkable, IRunable, ISwimable
    {
        string name;
        List<Movements> moves = new List<Movements>();
        public Giraffe()
        {
            name = "Giraffe";
            moves.Add(Movements.Run);
            moves.Add(Movements.Walk);
            moves.Add(Movements.Swim);
        }

        public void Move()
        {
            Console.Write("This is a " + name + " and it can ");
            foreach (Movements move in moves)
            {
                if (move == Movements.Walk)
                {
                    Walk();
                    Console.Write(", ");
                }
                else if (move == Movements.Run)
                {
                    Run();
                    Console.Write(", ");
                }
                else if(move == Movements.Swim)
                {
                    Swim();
                    Console.Write(", ");
                }
            }
            Console.WriteLine();
            Random r = new Random();
            Console.WriteLine("The " + name + " can " + moves[r.Next(moves.Count)]);
        }

        public void Run()
        {
            Console.Write("run");
        }

        public void Swim()
        {
            Console.Write("swim");
        }

        public void Walk()
        {
            Console.Write("walk");
        }
    }
}
