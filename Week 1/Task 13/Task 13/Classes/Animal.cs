﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13.Classes
{
    abstract class Animal
    {
        public enum Movements
        {
            Run,
            Walk,
            Climb,
            Swim,
            Fly
        }
    }
}
