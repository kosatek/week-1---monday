﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_13.Classes;

namespace Task_13
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("This task was completed by Lene and Eddie");
            Console.WriteLine();

            List<Animal> animals = new List<Animal>();

            animals.Add(new Cat());
            animals.Add(new Bird());
            animals.Add(new Giraffe());
            animals.Add(new Bird());

            foreach(Animal anim in animals)
            {
                ((IMovable)anim).Move();
                Console.WriteLine();
            }

            Console.Write("Press any key to exit...");
            Console.ReadLine();
        }
    }
}
