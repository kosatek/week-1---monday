﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_14.Classes;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This task was completed by Lene and Eddie");
            Console.WriteLine();

            List<Animal> animals = new List<Animal>();

            animals.Add(new Cat());
            animals.Add(new Bird());
            animals.Add(new Giraffe());
            animals.Add(new Bird());
            animals.Add(new Cat());
            animals.Add(new Giraffe());

            foreach (Animal anim in animals)
            {
                ((IMovable)anim).Move();
                Console.WriteLine();
            }

            animals.Sort();

            foreach(Animal anim in animals)
            {
                Console.WriteLine(anim.GetType().Name);
            }

            Console.Write("Press any key to exit...");
            Console.ReadLine();
        }
    }
}
