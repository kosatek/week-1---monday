﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14.Classes
{
    class Bird : Omnivore, IWalkable, IFlyable, ISwimable
    {
        string name;
        List<Movements> moves = new List<Movements>();
        public Bird()
        {
            name = "Bird";
            moves.Add(Movements.Fly);
            moves.Add(Movements.Swim);
            moves.Add(Movements.Walk);
        }

        public void Fly()
        {
            Console.Write("fly");
        }

        public void Move()
        {
            Console.Write("This is a " + name + " and it can ");
            foreach (Movements move in moves)
            {
                if (move == Movements.Fly)
                {
                    Fly();
                    Console.Write(", ");
                }
                else if (move == Movements.Swim)
                {
                    Swim();
                    Console.Write(", ");
                }
                else if (move == Movements.Walk)
                {
                    Walk();
                    Console.Write(", ");
                }
            }
            Console.WriteLine();
            Random r = new Random();
            Console.WriteLine("The " + name + " can " + moves[r.Next(moves.Count)]);
        }

        public void Swim()
        {
            Console.Write("swim");
        }

        public void Walk()
        {
            Console.Write("walk");
        }
    }
}
