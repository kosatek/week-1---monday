﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14.Classes
{
    class Cat : Carnivore, IWalkable, IRunable, IClimbable
    {
        string name;
        List<Movements> moves = new List<Movements>();

        public Cat()
        {
            name = "Cat";
            moves.Add(Movements.Run);
            moves.Add(Movements.Climb);
            moves.Add(Movements.Walk);
        }

        public void Climb()
        {
            Console.Write("climb");
        }

        public void Move()
        {
            Console.Write("This is a " + name + " and it can ");
            foreach (Movements move in moves)
            {
                if (move == Movements.Run)
                {
                    Run();
                    Console.Write(", ");
                }
                else if (move == Movements.Walk)
                {
                    Walk();
                    Console.Write(", ");
                }
                else if (move == Movements.Climb)
                {
                    Climb();
                    Console.Write(", ");
                }
            }
            Console.WriteLine();
            Random r = new Random();
            Console.WriteLine("The " + name + " can " + moves[r.Next(moves.Count)]);
        }

        public void Run()
        {
            Console.Write("run");
        }

        public void Walk()
        {
            Console.Write("walk");
        }
    }
}
