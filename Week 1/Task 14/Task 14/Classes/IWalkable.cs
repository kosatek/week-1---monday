﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14.Classes
{
    interface IWalkable : IMovable
    {
        void Walk();
    }
}
