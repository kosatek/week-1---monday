﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14.Classes
{
    abstract class Animal : IComparable
    {
        public int CompareTo(Object other)
        {

            if(other == null)
            {
                return 1;
            }
            if(other is Animal)
            {
                return GetType().Name.CompareTo(other.GetType().Name);
            }
            throw new ArgumentException("Wrong type, please use animals");
        }

        public enum Movements
        {
            Run,
            Walk,
            Climb,
            Swim,
            Fly
        }
    }
}
