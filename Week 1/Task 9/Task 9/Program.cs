﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_9
{
    class Animal
    {
        public Animal(string species, string race)
        {
            Species = species;
            Race = race;
        }

        public Animal(string species)
        {
            Species = species;
            Race = "Unknown";
        }

        public string Species { get; set; }
        public string Race { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>();

            animals.Add(new Animal("Dog", "Pincher"));
            animals.Add(new Animal("Cat", "Siamese"));
            animals.Add(new Animal("Fish", "Goldfish"));
            animals.Add(new Animal("Dog", "Rottweiler"));
            animals.Add(new Animal("Bird", "Canary"));
            animals.Add(new Animal("Bird")); //Overloaded constructor

            foreach(Animal animel in animals)
            {
                Console.WriteLine("Species: " + animel.Species + " | Race: " + animel.Race);
            }
        }
    }
}
