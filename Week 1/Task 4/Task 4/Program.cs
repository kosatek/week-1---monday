﻿using System;

namespace Task_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the dimensions you want the square to be");
            Console.Write("Column length: ");
            Int32.TryParse(Console.ReadLine(), out int col);

            Console.Write("Row length: ");
            Int32.TryParse(Console.ReadLine(), out int row);

            string[,] square = new string[col, row];

            for (int i = 0; i < col; i++)
            {
                for (int j = 0; j < row; j++)
                {
                    if (i == 0 || i == col - 1)
                    {
                        square[i, j] = "#";
                    }
                    else if(i == 2 || i == col - 3)
                    {
                        square[i, j] = "#";
                        if (j == 1 || j == row - 2)
                        {
                            square[i, j] = " ";
                        }
                        
                    }
                    else if(j == 2 || j == row - 3)
                    {
                        square[i, j] = "#";
                        if(i == 1 || i == col - 2)
                        {
                            square[i, j] = " ";
                        }
                    }
                    else
                    {
                        square[i, 0] = "#";

                        if (j != 0 && j != row - 1)
                        {
                            square[i, j] = " ";
                        }
                        square[i, row - 1] = "#";


                    }
                }
            }

            for (int i = 0; i < square.GetLength(0); i++)
            {
                for (int k = 0; k < square.GetLength(1); k++)
                {
                    Console.Write(square[i, k]);
                }
                Console.WriteLine();
            }

        }
    }
}
