﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaosArray
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Object> chaosArray = new object[10].ToList<Object>();
            Random rnd = new Random();
            int index = rnd.Next(0, chaosArray.Count()-1);

            int nextIndex = index;
            index = rnd.Next(0, chaosArray.Count()-1);

            try
            {
                if (!(index == nextIndex))
                {
                    int choise;
                    bool check = true;

                    while (check)
                    {
                        Console.WriteLine("Choose wether to add an int, string or object: ");
                        Console.WriteLine("1 - int");
                        Console.WriteLine("2 - string");
                        Console.WriteLine("3 - bool");
                        Console.WriteLine("4 - get a random element from the list");
                        Console.WriteLine("Anything else to close and print the list");
                        Int32.TryParse(Console.ReadLine(), out choise);
                        Console.WriteLine();

                        if (choise == 1 && !(index == nextIndex))
                        {
                            Console.Write("Pick a number: ");
                            chaosArray.Insert(index, Convert.ToInt32(Console.ReadLine()));
                            nextIndex = index;
                            index = rnd.Next(0, chaosArray.Count() - 1);
                        }
                        else if (choise == 2 && !(index == nextIndex))
                        {
                            Console.Write("Write a string: ");
                            chaosArray.Insert(index, Console.ReadLine());
                            nextIndex = index;
                            index = rnd.Next(0, chaosArray.Count() - 1);
                        }
                        else if (choise == 3 && !(index == nextIndex))
                        {
                            bool b;
                            Console.WriteLine("Pick a bool value:");
                            Console.WriteLine("1 - true");
                            Console.WriteLine("2 - false");
                            Int32.TryParse(Console.ReadLine(), out int setBool);
                            if (setBool == 1)
                            {
                                b = true;
                            }
                            else
                            {
                                b = false;
                            }
                            chaosArray.Insert(index, b);
                            nextIndex = index;
                            index = rnd.Next(0, chaosArray.Count() - 1);
                        }
                        else if(choise == 4)
                        {
                            getRandomElem(chaosArray, index);
                        }
                        else
                        {
                            foreach(Object ob in chaosArray)
                            {
                                if(ob == null)
                                {
                                    Console.WriteLine("NULL");
                                }
                                else
                                {
                                    Console.WriteLine(ob);
                                }
                            }
                            check = false;
                        }
                    }
                }
                else
                {
                    throw new CustomException("Index is already occupied");
                }
            }
            catch(IndexOutOfRangeException ie)
            {
                Console.WriteLine(ie.Message);
                throw new ArgumentOutOfRangeException("index parameter is out of range", ie);

            }
        }

        static void getRandomElem(List<Object> array, int index)
        {
            if(array[index] != null)
            {

            }
        }
    }
}
