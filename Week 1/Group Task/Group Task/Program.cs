﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Choose where to place your Queen: ");
            string input = Console.ReadLine();

            int[] position = ConvertInput(input);

            int x = position[0];
            int y = position[1] - 49;

            Solver(x, y);
        }
        static void Solver(int x, int y)
        {
            int[,] chessBoard1 = new int[8, 8]
            {
                { 0,0,0,1,0,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 0,0,1,0,0,0,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,1,0,0,0,0,0,0},
                { 0,0,0,0,1,0,0,0},
                { 1,0,0,0,0,0,0,0},
                { 0,0,0,0,0,1,0,0}
            };
            int[,] chessBoard2 = new int[8, 8]
            {
                { 0,0,0,0,1,0,0,0},
                { 0,1,0,0,0,0,0,0},
                { 0,0,0,1,0,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 0,0,1,0,0,0,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,0,0,0,0,1,0,0},
                { 1,0,0,0,0,0,0,0}
            };
            int[,] chessBoard3 = new int[8, 8]
            {
                { 0,0,0,1,0,0,0,0},
                { 0,1,0,0,0,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 0,0,1,0,0,0,0,0},
                { 0,0,0,0,0,1,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,0,0,0,1,0,0,0},
                { 1,0,0,0,0,0,0,0}
            };
            int[,] chessBoard4 = new int[8, 8]
            {
                { 0,0,0,1,0,0,0,0},
                { 0,0,0,0,0,1,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,0,1,0,0,0,0,0},
                { 1,0,0,0,0,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 0,0,0,0,1,0,0,0},
                { 0,1,0,0,0,0,0,0}
            };
            int[,] chessBoard5 = new int[8, 8]
            {
                { 0,0,1,0,0,0,0,0},
                { 0,0,0,0,0,1,0,0},
                { 0,0,0,0,0,0,0,1},
                { 1,0,0,0,0,0,0,0},
                { 0,0,0,1,0,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 0,0,0,0,1,0,0,0},
                { 0,1,0,0,0,0,0,0}
            };
            int[,] chessBoard6 = new int[8, 8]
            {
                { 0,0,0,0,1,0,0,0},
                { 0,0,1,0,0,0,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,0,0,1,0,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 1,0,0,0,0,0,0,0},
                { 0,0,0,0,0,1,0,0},
                { 0,1,0,0,0,0,0,0}
            };
            int[,] chessBoard7 = new int[8, 8]
            {
                { 0,0,0,0,1,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 0,0,0,1,0,0,0,0},
                { 1,0,0,0,0,0,0,0},
                { 0,0,1,0,0,0,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,0,0,0,0,1,0,0},
                { 0,1,0,0,0,0,0,0}
            };
            int[,] chessBoard8 = new int[8, 8]
            {
                { 0,0,0,1,0,0,0,0},
                { 1,0,0,0,0,0,0,0},
                { 0,0,0,0,1,0,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,0,0,0,0,1,0,0},
                { 0,0,1,0,0,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 0,1,0,0,0,0,0,0}
            };
            int[,] chessBoard9 = new int[8, 8]
            {
                { 0,0,1,0,0,0,0,0},
                { 0,0,0,0,0,1,0,0},
                { 0,0,0,1,0,0,0,0},
                { 1,0,0,0,0,0,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,0,0,0,1,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 0,1,0,0,0,0,0,0}
            };
            int[,] chessBoard10 = new int[8, 8]
            {
                { 0,0,0,0,0,1,0,0},
                { 0,1,0,0,0,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 1,0,0,0,0,0,0,0},
                { 0,0,0,1,0,0,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,0,0,0,1,0,0,0},
                { 0,0,1,0,0,0,0,0}
            };
            int[,] chessBoard11 = new int[8, 8]
            {
                { 0,0,0,1,0,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 1,0,0,0,0,0,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,0,0,0,1,0,0,0},
                { 0,1,0,0,0,0,0,0},
                { 0,0,0,0,0,1,0,0},
                { 0,0,1,0,0,0,0,0}
            };
            int[,] chessBoard12 = new int[8, 8]
            {
                { 0,0,0,0,0,1,0,0},
                { 0,0,0,1,0,0,0,0},
                { 0,0,0,0,0,0,1,0},
                { 1,0,0,0,0,0,0,0},
                { 0,0,0,0,0,0,0,1},
                { 0,1,0,0,0,0,0,0},
                { 0,0,0,0,1,0,0,0},
                { 0,0,1,0,0,0,0,0}
            };

            List<int[,]> boards = new List<int[,]>();

            boards.Add(chessBoard1);
            boards.Add(chessBoard2);
            boards.Add(chessBoard3);
            boards.Add(chessBoard4);
            boards.Add(chessBoard5);
            boards.Add(chessBoard6);
            boards.Add(chessBoard7);
            boards.Add(chessBoard8);
            boards.Add(chessBoard9);
            boards.Add(chessBoard10);
            boards.Add(chessBoard11);
            boards.Add(chessBoard12);

            int[,] check;
            int solutions = 0;

            for (int i = 0; i < boards.Count; i++)
            {
                check = boards[i];

                if(check[y, x] == 1)
                {
                    Console.WriteLine("Solution " + (++solutions) + ":");
                    for(int k = 0; k < 8; k++)
                    {
                        for(int s = 0; s < 8; s++)
                        {
                            Console.Write(check[k, s]);
                        }
                        Console.WriteLine();
                    }
                }
            }
            
            if(solutions == 0)
            {
                Console.WriteLine("There are no solutions that include a queen in the given position");
            }
        }
        static int[] ConvertInput(string input)
        {
            int[] result = new int[2];
            if (input.ToUpper().Contains("A"))
            {
                result[0] = 0;
                result[1] = Convert.ToInt32(input[1]);

                return result;
            }
            else if (input.ToUpper().Contains("B"))
            {
                result[0] = 1;
                result[1] = input[1];

                return result;
            }
            else if (input.ToUpper().Contains("C"))
            {
                result[0] = 2;
                result[1] = input[1];

                return result;
            }
            else if (input.ToUpper().Contains("D"))
            {
                result[0] = 3;
                result[1] = input[1];

                return result;
            }
            else if (input.ToUpper().Contains("E"))
            {
                result[0] = 4;
                result[1] = input[1];

                return result;
            }
            else if (input.ToUpper().Contains("F"))
            {
                result[0] = 5;
                result[1] = input[1];

                return result;
            }
            else if (input.ToUpper().Contains("G"))
            {
                result[0] = 6;
                result[1] = input[1];

                return result;
            }
            else if (input.ToUpper().Contains("H"))
            {
                result[0] = 7;
                result[1] = input[1];

                return result;
            }
            else
            {
                return null;
            }
        }
    }
}
