﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using Newtonsoft.Json;

namespace Task_19
{
    class Program
    {
        static string JSONString;
        //static string JSONStringInvoice;
        static string name;
        static Customers customer;
        static void Main(string[] args)
        {
            SQLiteConnection conn = new SQLiteConnection("Data Source=chinook.db; Version = 3; New = True;" +
                " Compress = True;");

            try
            {
                conn.Open();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteCommand sqlite_cmd = conn.CreateCommand();
            SQLiteDataReader sqlite_datareader;

            Random ran = new Random();
            int ranNum = ran.Next(1, 58);

            sqlite_cmd.CommandText = "SELECT c.CustomerId, c.FirstName, c.LastName, i.InvoiceId FROM customers c INNER JOIN invoices i ON i.CustomerId = c.CustomerId" +
                " WHERE c.CustomerId = " + ranNum + " ORDER BY i.InvoiceId ASC";

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            StreamWriter sw;

            Console.WriteLine("{0,-20} {1,5} {2,15} {3,25}\n", "CustomerID", "Firstname", "Lastname", "InvoiceID");
            Console.WriteLine("--------------------------------------------------------------------------------------");

            sqlite_datareader.Read();

            customer = new Customers(sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2));
            Invoices inv1 = new Invoices(sqlite_datareader.GetInt32(3));
            Console.WriteLine("{0,-20} {1,5} {2,20} {3,20}\n", sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetInt32(3));


            customer.addToInvoices(inv1);

            while (sqlite_datareader.Read())
            {
                Console.WriteLine("{0,-20} {1,5} {2,20} {3,20}\n", sqlite_datareader.GetInt32(0), sqlite_datareader.GetString(1), sqlite_datareader.GetString(2), sqlite_datareader.GetInt32(3));
                Invoices inv = new Invoices(sqlite_datareader.GetInt32(3));

                customer.addToInvoices(inv);    
               
                name = sqlite_datareader.GetString(1) + "_" + sqlite_datareader.GetString(2) + ".json";                            
            }
            JSONString = JsonConvert.SerializeObject(customer, Formatting.Indented);

            using (sw = new StreamWriter(name))
            {
                sw.WriteLine(JSONString);
            }
            Console.ReadLine();
            conn.Close();

        }
    }
}
