﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Task_19
{
    [JsonObject(MemberSerialization.Fields)]
    class Customers
    {
        private int customerId;
        private string firstName;
        private string lastName;
        private List<Invoices> invList;

        public Customers(int customerId, string firstName, string lastName)
        {
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
            invList = new List<Invoices>();
        }

        public void addToInvoices(Invoices inv)
        {
            invList.Add(inv);
        }

        public string LastName { get => lastName; set => lastName = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public int CustomerId { get => customerId; set => customerId = value; }
    }
}
