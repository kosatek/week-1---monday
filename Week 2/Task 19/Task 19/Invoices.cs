﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_19
{
    class Invoices
    {
        private int invoiceId;

        public Invoices(int invoiceId)
        {
            this.InvoiceId = invoiceId;
        }

        public int InvoiceId { get => invoiceId; set => invoiceId = value; }
    }
}
