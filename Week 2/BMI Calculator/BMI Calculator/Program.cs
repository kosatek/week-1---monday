﻿using System;

namespace BMI_Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("BMI-Calculator\n");
            Console.Write("Enter your weight in kg: ");
            double weight = Convert.ToDouble(Console.ReadLine());

            Console.Write("Enter your height in cm: ");
            double height = Convert.ToDouble(Console.ReadLine());

            double bmi = weight / ((height/100) * (height / 100)); //Dividing height with 100 to get it from cm to m.

            if(bmi < 18.5)
            {
                Console.WriteLine("Your BMI is: " + bmi);
                Console.WriteLine("You are underweight");
            }
            else if(bmi >= 18.5 && bmi < 25)
            {
                Console.WriteLine("Your BMI is: " + bmi);
                Console.WriteLine("You are normal weight");
            }
            else if(bmi >= 25 && bmi < 30)
            {
                Console.WriteLine("Your BMI is: " + bmi);
                Console.WriteLine("You are overweight");
            }
            else
            {
                Console.WriteLine("Your BMI is: " + bmi);
                Console.WriteLine("You are obese");
            }
        }
    }
}
