﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upgraded_Name_Search
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> names = new List<Person>();

            names.Add(new Person("Eddie", "Murphy", 911));
            names.Add(new Person("Tom",  "Cruise", 113));
            names.Add(new Person("Eddie", "Redmayne", 123));
            names.Add(new Person("Terry", "Crews", 321));
            names.Add(new Person("Rob", "Schneider"));

            while (true)
            {
                Console.WriteLine("\n1 - Search for a name\n2 - Display all names\n3 - Exit program");
                Int32.TryParse(Console.ReadLine(), out int choise);
                Console.WriteLine();

                if (choise == 1)
                {
                    Console.Write("Search: ");
                    string name = GetName(Console.ReadLine());

                    foreach (Person ppl in names)
                    {
                        ppl.FindPerson(ppl, name);
                    }          
                }
                else if (choise == 2)
                {
                    foreach (Person ppl in names)
                    {
                        Console.WriteLine(ppl.FName + " " + ppl.SName + " | " + ppl.Tlf);
                    }
                }
                else
                {
                    break;
                }
            }
        }
        
        /// <summary>
        /// Makes the first letter in the first name uppercase
        /// </summary>
        /// <param name="name"></param>
        /// <returns>string uppercaseString</returns>
        static string GetName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return string.Empty;
            }

            return char.ToUpper(name[0]) + name.Substring(1);
        }
    }
}
