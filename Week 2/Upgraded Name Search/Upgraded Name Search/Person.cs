﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Upgraded_Name_Search
{
    class Person
    {
        private string fName;
        private string sName;
        private int tlf;

        public Person(string fName, string sName, int tlf)
        {
            FName = fName;
            SName = sName;
            Tlf = tlf;
        }
        public Person(string fName, string sName)
        {
            FName = fName;
            SName = sName;
        }

        public string FName { get => fName; set => fName = value; }
        public string SName { get => sName; set => sName = value; }
        public int Tlf { get => tlf; set => tlf = value; }

        public void FindPerson(Person ppl, string name)
        {
            string fullName = ppl.FName + " " + ppl.SName;

            if (fullName.ToUpper().Contains(name.ToUpper()))
            {
                Console.WriteLine(ppl.FName + " " + ppl.SName + " | " + tlf);
            }
        }
    }
}
