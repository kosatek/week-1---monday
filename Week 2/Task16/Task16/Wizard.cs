﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task16
{
    public class Wizard
    {
        protected int hp;
        protected int mana;
        protected string name;
        protected int aRating;

        protected int Hp { get => hp; set => hp = value; }
        protected int Mana { get => mana; set => mana = value; }
        protected string Name { get => name; set => name = value; }
        protected int ARating { get => aRating; set => aRating = value; }

        public Wizard(int hp, int mana, string name, int aRating)
        {
            this.hp = hp;
            this.mana = mana;
            this.name = name;
            this.aRating = aRating;
        }
    }
}
