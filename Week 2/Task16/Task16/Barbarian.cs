﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task16
{
    public class Barbarian : Warrior
    {
        public Barbarian(int hp, int mana, string name, int aRating) : base(hp, mana, name, aRating)
        {

        }

        public string Attack()
        {
            string attack = name + " has attacked!";
            return attack;
        }
        public string Move()
        {
            string move = name + " moved!";
            return move;
        }
    }
}
