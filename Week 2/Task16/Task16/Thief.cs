﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task16
{
    public class Thief
    {
        protected int hp;
        protected int mana;
        protected string name;
        protected int aRating;

        protected int Hp { get => hp; set => hp = value; }
        protected int Mana { get => mana; set => mana = value; }
        protected string Name { get => name; set => name = value; }
        protected int ARating { get => aRating; set => aRating = value; }

        public Thief(int hp, int mana, string name, int aRating)
        {
            this.Hp = hp;
            this.Mana = mana;
            this.Name = name;
            this.ARating = aRating;
        }

        public Thief()
        {
        }
    }
}
