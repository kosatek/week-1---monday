﻿namespace Task16_v2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuperClass = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SubClass = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SetName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Attack = new System.Windows.Forms.Button();
            this.Move = new System.Windows.Forms.Button();
            this.CreateChar = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.SetHP = new System.Windows.Forms.TextBox();
            this.SetMana = new System.Windows.Forms.TextBox();
            this.SetArmor = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.DbDelete = new System.Windows.Forms.Button();
            this.DpCharacter = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.UpdateCharacter = new System.Windows.Forms.Button();
            this.AllCharacters = new System.Windows.Forms.ListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.OutputClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SuperClass
            // 
            this.SuperClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SuperClass.FormattingEnabled = true;
            this.SuperClass.Location = new System.Drawing.Point(184, 80);
            this.SuperClass.Name = "SuperClass";
            this.SuperClass.Size = new System.Drawing.Size(169, 24);
            this.SuperClass.TabIndex = 0;
            this.SuperClass.SelectedIndexChanged += new System.EventHandler(this.SuperClass_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Choose Super-class:";
            // 
            // SubClass
            // 
            this.SubClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SubClass.FormattingEnabled = true;
            this.SubClass.Location = new System.Drawing.Point(184, 163);
            this.SubClass.Name = "SubClass";
            this.SubClass.Size = new System.Drawing.Size(169, 24);
            this.SubClass.TabIndex = 1;
            this.SubClass.SelectedIndexChanged += new System.EventHandler(this.SubClass_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "Choose Sub-class:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(536, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Set HP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(520, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Set Mana:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(390, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(202, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Enter Unique Character Name:";
            // 
            // SetName
            // 
            this.SetName.Location = new System.Drawing.Point(598, 189);
            this.SetName.Name = "SetName";
            this.SetName.Size = new System.Drawing.Size(182, 22);
            this.SetName.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(497, 243);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Armor Rating:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(59, 230);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(309, 214);
            this.textBox2.TabIndex = 12;
            this.textBox2.TextChanged += new System.EventHandler(this.TextBox2_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(59, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "Character Output:";
            // 
            // Attack
            // 
            this.Attack.Location = new System.Drawing.Point(461, 355);
            this.Attack.Name = "Attack";
            this.Attack.Size = new System.Drawing.Size(75, 28);
            this.Attack.TabIndex = 8;
            this.Attack.Text = "Attack";
            this.Attack.UseVisualStyleBackColor = true;
            this.Attack.Click += new System.EventHandler(this.Attack_Click);
            // 
            // Move
            // 
            this.Move.Location = new System.Drawing.Point(461, 389);
            this.Move.Name = "Move";
            this.Move.Size = new System.Drawing.Size(75, 28);
            this.Move.TabIndex = 10;
            this.Move.Text = "Move";
            this.Move.UseVisualStyleBackColor = true;
            this.Move.Click += new System.EventHandler(this.Move_Click);
            // 
            // CreateChar
            // 
            this.CreateChar.Location = new System.Drawing.Point(461, 291);
            this.CreateChar.Name = "CreateChar";
            this.CreateChar.Size = new System.Drawing.Size(176, 29);
            this.CreateChar.TabIndex = 6;
            this.CreateChar.Text = "Create Character";
            this.CreateChar.UseVisualStyleBackColor = true;
            this.CreateChar.Click += new System.EventHandler(this.Button3_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(397, 323);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(457, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "This will print out info in \"character output\" and will write the info to a file";
            // 
            // SetHP
            // 
            this.SetHP.Location = new System.Drawing.Point(598, 82);
            this.SetHP.Name = "SetHP";
            this.SetHP.Size = new System.Drawing.Size(100, 22);
            this.SetHP.TabIndex = 2;
            this.SetHP.TextChanged += new System.EventHandler(this.SetHP_TextChanged);
            // 
            // SetMana
            // 
            this.SetMana.Location = new System.Drawing.Point(598, 131);
            this.SetMana.Name = "SetMana";
            this.SetMana.Size = new System.Drawing.Size(100, 22);
            this.SetMana.TabIndex = 3;
            // 
            // SetArmor
            // 
            this.SetArmor.Location = new System.Drawing.Point(598, 243);
            this.SetArmor.Name = "SetArmor";
            this.SetArmor.Size = new System.Drawing.Size(100, 22);
            this.SetArmor.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(354, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(212, 17);
            this.label9.TabIndex = 21;
            this.label9.Text = "Made by Eddie, using Lene\'s .dll";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(397, 355);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 17);
            this.label10.TabIndex = 22;
            this.label10.Text = "Actions:";
            // 
            // DbDelete
            // 
            this.DbDelete.Location = new System.Drawing.Point(704, 355);
            this.DbDelete.Name = "DbDelete";
            this.DbDelete.Size = new System.Drawing.Size(130, 28);
            this.DbDelete.TabIndex = 9;
            this.DbDelete.Text = "Delete from DB";
            this.DbDelete.UseVisualStyleBackColor = true;
            this.DbDelete.Click += new System.EventHandler(this.DbDelete_Click);
            // 
            // DpCharacter
            // 
            this.DpCharacter.Location = new System.Drawing.Point(658, 291);
            this.DpCharacter.Name = "DpCharacter";
            this.DpCharacter.Size = new System.Drawing.Size(176, 28);
            this.DpCharacter.TabIndex = 7;
            this.DpCharacter.Text = "Display All Characters";
            this.DpCharacter.UseVisualStyleBackColor = true;
            this.DpCharacter.Click += new System.EventHandler(this.DpCharacter_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(575, 355);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 17);
            this.label11.TabIndex = 25;
            this.label11.Text = "Database Actions:";
            // 
            // UpdateCharacter
            // 
            this.UpdateCharacter.Location = new System.Drawing.Point(704, 389);
            this.UpdateCharacter.Name = "UpdateCharacter";
            this.UpdateCharacter.Size = new System.Drawing.Size(130, 28);
            this.UpdateCharacter.TabIndex = 11;
            this.UpdateCharacter.Text = "Update Character";
            this.UpdateCharacter.UseVisualStyleBackColor = true;
            this.UpdateCharacter.Click += new System.EventHandler(this.UpdateCharacter_Click);
            // 
            // AllCharacters
            // 
            this.AllCharacters.FormattingEnabled = true;
            this.AllCharacters.ItemHeight = 16;
            this.AllCharacters.Location = new System.Drawing.Point(864, 207);
            this.AllCharacters.Name = "AllCharacters";
            this.AllCharacters.Size = new System.Drawing.Size(415, 228);
            this.AllCharacters.TabIndex = 27;
            this.AllCharacters.SelectedIndexChanged += new System.EventHandler(this.AllCharacters_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(859, 175);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(269, 29);
            this.label12.TabIndex = 28;
            this.label12.Text = "All Characters Display";
            // 
            // OutputClear
            // 
            this.OutputClear.Location = new System.Drawing.Point(293, 204);
            this.OutputClear.Name = "OutputClear";
            this.OutputClear.Size = new System.Drawing.Size(75, 23);
            this.OutputClear.TabIndex = 15;
            this.OutputClear.Text = "Clear";
            this.OutputClear.UseVisualStyleBackColor = true;
            this.OutputClear.Click += new System.EventHandler(this.OutputClear_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1291, 450);
            this.Controls.Add(this.OutputClear);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.AllCharacters);
            this.Controls.Add(this.UpdateCharacter);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.DpCharacter);
            this.Controls.Add(this.DbDelete);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.SetArmor);
            this.Controls.Add(this.SetMana);
            this.Controls.Add(this.SetHP);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.CreateChar);
            this.Controls.Add(this.Move);
            this.Controls.Add(this.Attack);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.SetName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SubClass);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SuperClass);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox SuperClass;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox SubClass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox SetName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button Attack;
        private System.Windows.Forms.Button Move;
        private System.Windows.Forms.Button CreateChar;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox SetHP;
        private System.Windows.Forms.TextBox SetMana;
        private System.Windows.Forms.TextBox SetArmor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button DbDelete;
        private System.Windows.Forms.Button DpCharacter;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button UpdateCharacter;
        private System.Windows.Forms.ListBox AllCharacters;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button OutputClear;
    }
}

