﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Task16;
using System.Data.SQLite;

namespace Task16_v2
{
    public partial class Form1 : Form
    {
        SQLiteConnection conn = new SQLiteConnection("Data Source=Task17.db; Version = 3; New = True;" +
                "Compress = true;");
        public Form1()
        {
            InitializeComponent();
            SuperClass.Items.Add("Warrior");
            SuperClass.Items.Add("Wizard");
            SuperClass.Items.Add("Thief");

            try
            {
                conn.Open();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd = conn.CreateCommand();

            sqlite_cmd.CommandText = "SELECT c.characterId, c.name, c.hp, c.mana, c.armor, cl.name" +
                " FROM Character c" +
                " INNER JOIN Class cl ON cl.classId = c.classId";
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                AllCharacters.Items.Add(sqlite_datareader.GetInt32(0) + "\t" + sqlite_datareader.GetString(1) +
                    "\t" + sqlite_datareader.GetInt32(2) + "\t" + sqlite_datareader.GetInt32(3) + "\t" +
                    sqlite_datareader.GetInt32(4) + "\t" + sqlite_datareader.GetString(5));
            }

            conn.Close();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Int32.TryParse(SetHP.Text, out int hp);
            Int32.TryParse(SetMana.Text, out int mana);
            Int32.TryParse(SetArmor.Text, out int armor);
            string name = SetName.Text;

            if(SuperClass.SelectedIndex == 0)
            {
                if(SubClass.SelectedIndex == 0)
                {
                    Warrior war = new Marauder(hp, mana, name, armor);
                    WriteCharStatsToTextbox(hp, mana, name, armor, war);
                    WriteToFile(hp, mana, name, armor, war);
                    InsertIntoTable(hp, mana, name, armor);
                }
                else if(SubClass.SelectedIndex == 1)
                {
                    Warrior war = new Barbarian(hp, mana, name, armor);
                    WriteCharStatsToTextbox(hp, mana, name, armor, war);
                    WriteToFile(hp, mana, name, armor, war);
                    InsertIntoTable(hp, mana, name, armor);
                }
                else
                {
                    textBox2.Text = "Please pick a Sub-Class!";
                }
            }
            else if(SuperClass.SelectedIndex == 1)
            {
                if(SubClass.SelectedIndex == 0)
                {
                    Wizard wiz = new Conjurer(hp, mana, name, armor);
                    WriteCharStatsToTextbox(hp, mana, name, armor, wiz);
                    WriteToFile(hp, mana, name, armor, wiz);
                    InsertIntoTable(hp, mana, name, armor);
                }
                else if(SubClass.SelectedIndex == 1)
                {
                    Wizard wiz = new Divination(hp, mana, name, armor);
                    WriteCharStatsToTextbox(hp, mana, name, armor, wiz);
                    WriteToFile(hp, mana, name, armor, wiz);
                    InsertIntoTable(hp, mana, name, armor);
                }
                else
                {
                    textBox2.Text = "Please pick a Sub-Class!";
                }
            }
            else if(SuperClass.SelectedIndex == 2)
            {
                if (SubClass.SelectedIndex == 0)
                {
                    Thief thf = new Burglar(hp, mana, name, armor);
                    WriteCharStatsToTextbox(hp, mana, name, armor, thf);
                    WriteToFile(hp, mana, name, armor, thf);
                    InsertIntoTable(hp, mana, name, armor);
                }
                else if (SubClass.SelectedIndex == 1)
                {
                    Thief thf = new Rogue(hp, mana, name, armor);
                    WriteCharStatsToTextbox(hp, mana, name, armor, thf);
                    WriteToFile(hp, mana, name, armor, thf);
                    InsertIntoTable(hp, mana, name, armor);
                }
                else
                {
                    textBox2.Text = "Please pick a Sub-Class!";
                }
            }
            DpCharacter_Click(sender, e);
        }

        private void InsertIntoTable(int hp, int mana, string name, int armor)
        {
            try
            {
                conn.Open();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            int classId = 0;

            if (SuperClass.SelectedIndex == 0)
            {
                classId = 1;
            }
            else if (SuperClass.SelectedIndex == 1)
            {
                classId = 2;
            }
            else if (SuperClass.SelectedIndex == 2)
            {
                classId = 3;
            }

            if (classId == 0)
            {
                textBox2.Text = "Please pick a superclass!";
            }
            else
            {

                SQLiteCommand sqlite_cmd = conn.CreateCommand();

                sqlite_cmd.CommandText = "INSERT INTO Character(characterId, name, hp, mana, armor, classId)" +
                    "VALUES ((SELECT MAX(characterId+1) FROM Character), @name, @hp, @mana, @armor, @classId);";

                sqlite_cmd.Parameters.AddWithValue("@name", name);
                sqlite_cmd.Parameters.AddWithValue("@hp", hp);
                sqlite_cmd.Parameters.AddWithValue("@mana", mana);
                sqlite_cmd.Parameters.AddWithValue("@armor", armor);
                sqlite_cmd.Parameters.AddWithValue("@classId", classId);

                sqlite_cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        private void WriteCharStatsToTextbox(int hp, int mana, string name, int armor, object player)
        {
            if (player == null)
            {
                Console.WriteLine("Error: You chose no class!");
            }
            else
            {
                textBox2.ResetText();
                textBox2.Text += "Character class: " + player.GetType().BaseType.Name;
                textBox2.AppendText(Environment.NewLine);

                textBox2.Text += "Character Sub-Class: " + player.GetType().Name;
                textBox2.AppendText(Environment.NewLine);

                textBox2.Text += "Character Name: " + name;
                textBox2.AppendText(Environment.NewLine);

                textBox2.Text += "Health Points: " + hp;
                textBox2.AppendText(Environment.NewLine);

                textBox2.Text += "Mana Points: " + mana;
                textBox2.AppendText(Environment.NewLine);

                textBox2.Text += "Armor Points: " + armor;
                textBox2.AppendText(Environment.NewLine);
            }
        }
        private void WriteToFile(int hp, int mana, string name, int armor, object player)
        {

            using(StreamWriter sw = new StreamWriter("Char-Stats.txt"))
            {
                sw.WriteLine("Character Class: " + player.GetType().BaseType.Name);
                sw.WriteLine("Character Sub-class: " + player.GetType().Name);
                sw.WriteLine("Character Name: " + name);
                sw.WriteLine("Health Points: " + hp);
                sw.WriteLine("Mana Points: " + mana);
                sw.WriteLine("Armor Points: " + armor);
            }

        }

        private void SuperClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SuperClass.SelectedIndex == 0)
            {
                SubClass.Items.Clear();
                SubClass.Items.Add("Marauder");
                SubClass.Items.Add("Barbarian");
            }
            else if(SuperClass.SelectedIndex == 1)
            {
                SubClass.Items.Clear();
                SubClass.Items.Add("Conjurer");
                SubClass.Items.Add("Divination");
            }
            else if(SuperClass.SelectedIndex == 2)
            {
                SubClass.Items.Clear();
                SubClass.Items.Add("Burglar");
                SubClass.Items.Add("Rogue");
            }
        }

        private void SubClass_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void SetHP_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Attack_Click(object sender, EventArgs e)
        {
            Int32.TryParse(SetHP.Text, out int hp);
            Int32.TryParse(SetMana.Text, out int mana);
            Int32.TryParse(SetArmor.Text, out int armor);
            string name = SetName.Text;

            if (SuperClass.SelectedIndex == 0)
            {
                if(SubClass.SelectedIndex == 0)
                {
                    Marauder war = new Marauder(hp, mana, name, armor);
                    textBox2.Text += war.Attack();
                    textBox2.AppendText(Environment.NewLine);
                }
                else if(SubClass.SelectedIndex == 1)
                {
                    Barbarian war = new Barbarian(hp, mana, name, armor);
                    textBox2.Text += war.Attack();
                    textBox2.AppendText(Environment.NewLine);
                }
                else
                {
                    textBox2.Text = "Please choose a Sub-Class!";
                }
            }
            else if (SuperClass.SelectedIndex == 1)
            {
                if (SubClass.SelectedIndex == 0)
                {
                    Conjurer wiz = new Conjurer(hp, mana, name, armor);
                    textBox2.Text += wiz.Attack();
                    textBox2.AppendText(Environment.NewLine);
                }
                else if (SubClass.SelectedIndex == 1)
                {
                    Divination wiz = new Divination(hp, mana, name, armor);
                    textBox2.Text += wiz.Attack();
                    textBox2.AppendText(Environment.NewLine);
                }
                else
                {
                    textBox2.Text = "Please choose a Sub-Class!";
                }
            }
            else if (SuperClass.SelectedIndex == 2)
            {
                if (SubClass.SelectedIndex == 0)
                {
                    Burglar thf = new Burglar(hp, mana, name, armor);
                    textBox2.Text += thf.Attack();
                    textBox2.AppendText(Environment.NewLine);
                }
                else if (SubClass.SelectedIndex == 1)
                {
                    Rogue thf = new Rogue(hp, mana, name, armor);
                    textBox2.Text += thf.Attack();
                    textBox2.AppendText(Environment.NewLine);
                }
                else
                {
                    textBox2.Text = "Please choose a Sub-Class!";
                }
            }
        }

        private void Move_Click(object sender, EventArgs e)
        {
            Int32.TryParse(SetHP.Text, out int hp);
            Int32.TryParse(SetMana.Text, out int mana);
            Int32.TryParse(SetArmor.Text, out int armor);
            string name = SetName.Text;

            if (SuperClass.SelectedIndex == 0)
            {
                if (SubClass.SelectedIndex == 0)
                {
                    Marauder war = new Marauder(hp, mana, name, armor);
                    textBox2.Text += war.Move();
                    textBox2.AppendText(Environment.NewLine);
                }
                else if (SubClass.SelectedIndex == 1)
                {
                    Barbarian war = new Barbarian(hp, mana, name, armor);
                    textBox2.Text += war.Move();
                    textBox2.AppendText(Environment.NewLine);
                }
                else
                {
                    textBox2.Text = "Please choose a Sub-Class!";
                }
            }
            else if (SuperClass.SelectedIndex == 1)
            {
                if (SubClass.SelectedIndex == 0)
                {
                    Conjurer wiz = new Conjurer(hp, mana, name, armor);
                    textBox2.Text += wiz.Move();
                    textBox2.AppendText(Environment.NewLine);
                }
                else if (SubClass.SelectedIndex == 1)
                {
                    Divination wiz = new Divination(hp, mana, name, armor);
                    textBox2.Text += wiz.Move();
                    textBox2.AppendText(Environment.NewLine);
                }
                else
                {
                    textBox2.Text = "Please choose a Sub-Class!";
                }
            }
            else if (SuperClass.SelectedIndex == 2)
            {
                if (SubClass.SelectedIndex == 0)
                {
                    Burglar thf = new Burglar(hp, mana, name, armor);
                    textBox2.Text += thf.Move();
                    textBox2.AppendText(Environment.NewLine);
                }
                else if (SubClass.SelectedIndex == 1)
                {
                    Rogue thf = new Rogue(hp, mana, name, armor);
                    textBox2.Text += thf.Move();
                    textBox2.AppendText(Environment.NewLine);
                }
                else
                {
                    textBox2.Text = "Please choose a Sub-Class!";
                }
            }
        }

        private void UpdateCharacter_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Int32.TryParse(SetHP.Text, out int hp);
            Int32.TryParse(SetMana.Text, out int mana);
            Int32.TryParse(SetArmor.Text, out int armor);
            string name = SetName.Text;
            int classId = 0;
            object player = null;

            if (SuperClass.SelectedIndex == 0)
            {
                classId = 1;
                if(SubClass.SelectedIndex == 0)
                {
                    player = new Marauder(hp, mana, name, armor);
                }
                else if(SubClass.SelectedIndex == 1)
                {
                    player = new Barbarian(hp, mana, name, armor);
                }
            }
            else if (SuperClass.SelectedIndex == 1)
            {
                classId = 2;
                if(SubClass.SelectedIndex == 0)
                {
                    player = new Conjurer(hp, mana, name, armor);
                }
                else if(SubClass.SelectedIndex == 1)
                {
                    player = new Divination(hp, mana, name, armor);
                }
            }
            else if (SuperClass.SelectedIndex == 2)
            {
                classId = 3;
                if (SubClass.SelectedIndex == 0)
                {
                    player = new Burglar(hp, mana, name, armor);
                }
                else if (SubClass.SelectedIndex == 1)
                {
                    player = new Rogue(hp, mana, name, armor);
                }
            }

            if (classId == 0)
            {
                textBox2.Text = "Please choose a superclass and subclass!";
            }
            else
            {
                SQLiteCommand sqlite_cmd = conn.CreateCommand();

                sqlite_cmd.CommandText = "UPDATE Character" +
                    " SET name = @name, hp = @hp, mana = @mana, armor = @armor, classId = @classId" +
                    " WHERE name = @name";

                sqlite_cmd.Parameters.AddWithValue("@name", name);
                sqlite_cmd.Parameters.AddWithValue("@hp", hp);
                sqlite_cmd.Parameters.AddWithValue("@mana", mana);
                sqlite_cmd.Parameters.AddWithValue("@armor", armor);
                sqlite_cmd.Parameters.AddWithValue("@classId", classId);

                sqlite_cmd.ExecuteNonQuery();

                WriteCharStatsToTextbox(hp, mana, name, armor, player);
                conn.Close();
                DpCharacter_Click(sender, e);
            }
        }

        private void DbDelete_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            string name = SetName.Text;

            SQLiteCommand sqlite_cmd = conn.CreateCommand();

            sqlite_cmd.CommandText = "DELETE FROM Character " +
                "WHERE name = @name";

            sqlite_cmd.Parameters.AddWithValue("@name", name);

            sqlite_cmd.ExecuteNonQuery();
            textBox2.Text += "Delete completed";
            textBox2.AppendText(Environment.NewLine);
            conn.Close();
            DpCharacter_Click(sender, e);
        }

        private void DpCharacter_Click(object sender, EventArgs e)
        {

            AllCharacters.Items.Clear();
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd = conn.CreateCommand();

            sqlite_cmd.CommandText = "SELECT c.characterId, c.name, c.hp, c.mana, c.armor, cl.name" +
                " FROM Character c" +
                " INNER JOIN Class cl ON cl.classId = c.classId";
            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                AllCharacters.Items.Add(sqlite_datareader.GetInt32(0) + "\t" + sqlite_datareader.GetString(1) + 
                    "\t" + sqlite_datareader.GetInt32(2) + "\t" + sqlite_datareader.GetInt32(3) + "\t" +
                    sqlite_datareader.GetInt32(4) + "\t" + sqlite_datareader.GetString(5));
            }

            conn.Close();
        }

        private void AllCharacters_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void OutputClear_Click(object sender, EventArgs e)
        {
            textBox2.ResetText();
        }
    }
}
