﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMI_Upgrade
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("BMI-Calculator\n");

            double weight = InitWeight();
            double height = InitHeight();

            double bmi = weight / ((height / 100) * (height / 100)); //Dividing height with 100 to get it from cm to m.

            PrintBMI(bmi);
        }

        /// <summary>
        /// Initialises weight
        /// </summary>
        /// <returns>weight</returns>
        static double InitWeight()
        {
            Console.Write("Enter your weight in kg: ");
            double weight = Convert.ToDouble(Console.ReadLine());

            return weight;
        }
        /// <summary>
        /// Initialises height
        /// </summary>
        /// <returns>height</returns>
        static double InitHeight()
        {
            Console.Write("Enter your height in cm: ");
            double height = Convert.ToDouble(Console.ReadLine());

            return height;
        }
        /// <summary>
        /// Prints out which demographic the given BMI belongs in
        /// </summary>
        /// <param name="bmi"></param>
        static void PrintBMI(double bmi)
        {
            if (bmi < 18.5)
            {
                Console.WriteLine("Your BMI is: " + bmi);
                Console.WriteLine("You are underweight");
            }
            else if (bmi >= 18.5 && bmi < 25)
            {
                Console.WriteLine("Your BMI is: " + bmi);
                Console.WriteLine("You are normal weight");
            }
            else if (bmi >= 25 && bmi < 30)
            {
                Console.WriteLine("Your BMI is: " + bmi);
                Console.WriteLine("You are overweight");
            }
            else
            {
                Console.WriteLine("Your BMI is: " + bmi);
                Console.WriteLine("You are obese");
            }
        }
    }
}
