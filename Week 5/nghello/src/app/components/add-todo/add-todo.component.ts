import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TodoItem } from 'src/app/models/todo-item.model';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent implements OnInit {

  @Output() addTodo: EventEmitter<TodoItem>;

  todo = {} as TodoItem;


  constructor() { 
    this.addTodo = new EventEmitter<TodoItem>();
  }

  ngOnInit() {
  }

  onAddClicked() {
    if(this.todo.title && this.todo.title.length > 2){
      this.todo.completed = false;
      this.todo.created_at = Date.now().toString();
      this.addTodo.emit(this.todo);
      this.todo = {} as TodoItem;
    }
    else{
      alert('Please enter a todo first');
    }
  }
}
