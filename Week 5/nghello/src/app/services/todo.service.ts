import { Injectable } from '@angular/core';
import { TodoItem } from '../models/todo-item.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private todos: TodoItem[] = [];
  private todos$: BehaviorSubject<TodoItem[]> = new BehaviorSubject<TodoItem[]>([]);

  constructor() { }

  getTodos$(): Observable<TodoItem[]>{
    return this.todos$.asObservable();
  }

  addTodos(newTodo:TodoItem){
    newTodo.id = this.nextId();
    this.todos.push(newTodo);
    this.todos$.next(this.todos);
  }

  updateTodo(todo: TodoItem){
    for(let i = 0; i < this.todos.length; i++){
      if(this.todos[i].id === todo.id){
        this.todos[i] = todo;
        this.todos[i].edit = false;
        break;
      }
    }
    this.todos$.next(this.todos)
  }

  deleteTodo(id){
    this.todos = this.todos.filter(todo => todo.id !== id);
    this.todos$.next(this.todos);
  }

  setEditabled(id){
    for(let i = 0; i < this.todos.length; i++){
      if(this.todos[i].id === id){
        this.todos[i].edit = true;
        break;
      }
    }
    this.todos$.next(this.todos);
  }

  private nextId(){
    return this.todos.length > 0 ? this.todos[this.todos.length-1].id + 1 : 1;
  }
}
